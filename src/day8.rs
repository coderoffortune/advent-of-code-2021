#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Entry {
    signal_pattern: Vec<String>,
    digits: Vec<String>
}

#[aoc_generator(day8)]
fn input_generator(input: &str) -> Vec<Entry> {
    input
        .lines()
        .map(|l| {
            let data: Vec<Vec<String>> = l.trim()
                .split("|")
                .map(|segment|
                    segment
                        .trim()
                        .split(" ")
                        .map(|e| e.to_string())
                        .collect()
                )
                .collect();
            
            Entry {
                signal_pattern: data[0].clone(),
                digits: data[1].clone(),
            }
        })
        .collect()
}

#[aoc(day8, part1)]
fn day8_1(input: &Vec<Entry>) -> usize {
    input
        .iter()
        .flat_map(|entry| entry.digits.clone())
        .filter(|digit| digit.len() != 6 && digit.len() != 5)
        .collect::<Vec<String>>()
        .len()
}

// #[aoc(day8, part2)]
// fn day8_2(input: &Vec<Entry>) -> i32 {

//     61229
// }

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day8_1() {
        let input = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
        edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
        fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
        fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
        aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
        fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
        dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
        bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
        egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
        gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
        
        let parsed_input = input_generator(input);
        assert_eq!(day8_1(&parsed_input), 26);
    }

    #[test]
    fn test_day8_2() {
        let input = "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
        edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
        fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
        fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
        aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
        fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
        dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
        bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
        egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
        gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";

        let parsed_input = input_generator(input);
        assert_eq!(day8_2(&parsed_input), 61229);
    }
}
