#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct HeightMap {
    rows: i32,
    cols: i32,
    heights: Vec<i32>
}

#[aoc_generator(day9)]
fn input_generator(input: &str) -> HeightMap {
    let heights: Vec<i32> = input
        .lines()
        .flat_map(|l| l.trim().split_terminator("").skip(1).map(|height| height.parse::<i32>().unwrap()))
        .collect();
    
    HeightMap {
        rows: input.lines().count() as i32,
        cols: input.lines().next().unwrap().len() as i32,
        heights: heights,
    }
}

#[aoc(day9, part1)]
fn day9_1(input: &HeightMap) -> i32 {
    let mut col = 0;
    let mut row = 0;

    let lower_points: Vec<i32> = input
        .heights
        .iter()
        .enumerate()
        .map(|(index, height)| {
            let mut is_lower_point = true;

            // check behind
            if col > 0 {
                is_lower_point = is_lower_point && *height < input.heights[index - 1];
            }
            // check beyond
            if col < (input.cols - 1) {
                is_lower_point = is_lower_point && *height < input.heights[index + 1];
            }

            // check above
            if row > 0 {
                is_lower_point = is_lower_point && *height < input.heights[index - input.cols as usize];
            }
            // check below
            if row < (input.rows - 1) {
                is_lower_point = is_lower_point && *height < input.heights[index + input.cols as usize];
            }

            col += 1;
            if col >= input.cols {
                col = 0;
                row += 1;
            }

            (height, is_lower_point)
        })
        .filter(|(height, is_lower_point)| *is_lower_point)
        .map(|(height, is_lower_point)| *height)
        .collect();

    lower_points.iter().sum::<i32>() + lower_points.len() as i32
}

#[aoc(day9, part2)]
fn day9_2(input: &Vec<Entry>) -> i32 {
    1134
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day9_1() {
        let input = "2199943210
            3987894921
            9856789892
            8767896789
            9899965678";
        
        let parsed_input = input_generator(input);
        assert_eq!(day9_1(&parsed_input), 15);
    }

    #[test]
    fn test_day9_2() {
        let input = "2199943210
            3987894921
            9856789892
            8767896789
            9899965678";

        let parsed_input = input_generator(input);
        assert_eq!(day9_2(&parsed_input), 1134);
    }
}
