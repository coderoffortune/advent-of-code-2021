#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<i32> {
    input
        .lines()
        .map(|l| l.trim())
        .map(|l| l.parse::<i32>().unwrap())
        .collect()
}

#[aoc(day1, part1)]
fn day1_1(input: &Vec<i32>) -> usize {
    let mut phased_input = input.to_vec();
    phased_input.drain(0..1);

    return input.iter().zip(phased_input.iter()).filter(|(a,b)| b > a ).count();
}

#[aoc(day1, part2)]
fn day1_2(input: &Vec<i32>) -> usize {
    let mut phased_input = input.to_vec();
    phased_input.drain(0..3);

    return input.iter().zip(phased_input.iter()).filter(|(a,b)| b > a ).count();
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day1_1() {
        let input = "199
            200
            208
            210
            200
            207
            240
            269
            260
            263";

        let parsed_input = input_generator(input);
        assert_eq!(day1_1(&parsed_input), 7);
    }

    #[test]
    fn test_day1_2() {
        let input = "199
            200
            208
            210
            200
            207
            240
            269
            260
            263";

        let parsed_input = input_generator(input);
        assert_eq!(day1_2(&parsed_input), 5);
    }
}