use std::str::FromStr;

#[derive(Debug, PartialEq)]
enum Direction {
    Forward,
    Up,
    Down
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(input: &str) -> Result<Direction, Self::Err> {
        match input {
            "forward"  => Ok(Direction::Forward),
            "up"       => Ok(Direction::Up),
            "down"     => Ok(Direction::Down),
            _          => Err(()),
        }
    }
}

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Vec<(Direction, i32)> {
    input
        .lines()
        .flat_map(|l| l.trim().split_once(" "))
        .map(|(dir, amount)| (Direction::from_str(dir).unwrap(), amount.parse::<i32>().unwrap()))
        .collect()
}

#[aoc(day2, part1)]
fn day2_1(input: &Vec<(Direction, i32)>) -> u32 {
    let (h, v) = input.iter().fold(
        (0, 0),
        |(x, y), (direction, amount)| {
            match direction {
                Direction::Forward  => (x + amount, y),
                Direction::Up       => (x, y - amount),
                Direction::Down     => (x, y + amount),
            }
        }
    );

    return (h as u32) * (v as u32);
}

#[aoc(day2, part2)]
fn day2_2(input: &Vec<(Direction, i32)>) -> u32 {
    let (pos, depth, _) = input.iter().fold(
        (0, 0, 0),
        |(x, y, aim), (direction, amount)| {
            match direction {
                Direction::Forward  => (x + amount, y + aim * amount, aim),
                Direction::Up       => (x, y, aim - amount),
                Direction::Down     => (x, y, aim + amount),
            }
        }
    );

    return (pos as u32) * (depth as u32);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day2_1() {
        let input = "forward 5
                     down 5
                     forward 8
                     up 3
                     down 8
                     forward 2";
                                       
        assert_eq!(day2_1(&input_generator(input)), 150);
    }

    #[test]
    fn test_day2_2() {
        let input = "forward 5
                     down 5
                     forward 8
                     up 3
                     down 8
                     forward 2";
                                       
        assert_eq!(day2_2(&input_generator(input)), 900);
    }
}
