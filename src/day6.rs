use std::collections::HashMap;

fn frequency_map(nums: &Vec<i32>) -> HashMap<i32, u64> {
    let mut map = HashMap::new();

    for &n in nums {
        *map.entry(n).or_insert(0) += 1;
    }
    
    map
}

#[aoc_generator(day6)]
fn input_generator(input: &str) -> HashMap<i32, u64> {
    let spawn_times: Vec<i32> = input
        .trim()
        .split(",")
        .map(|lifespan| lifespan.trim().parse::<i32>().unwrap())
        .collect();

    frequency_map(&spawn_times)
}

fn generation_reproductivity(first_spawn_time: i32, days: i32, gen: i32) -> i32 {
    let new_born_spawn_time: i32 = 9;
    let standard_spawn_time: i32 = 7;

    let dividend = days - (first_spawn_time + 1) - new_born_spawn_time * gen;

    if dividend < 0 {
        return 0
    }
    
    dividend / standard_spawn_time + 1
}

fn generation_growth(seed: &Vec<i32>, reproductions: usize) -> Vec<i32> {
    seed[0..=reproductions-1]
        .iter()
        .fold((Vec::new(), 0), |(growth, base), n| 
            ([&growth[..], &vec![base+n]].concat(), base+n) 
        ).0
}

fn lanternfish_growth(first_spawn_time: i32, days: i32) -> u64 {
    let mut generations: Vec<i32> = Vec::new();
    let mut current_gen_growth = generation_reproductivity(first_spawn_time, days, 0);
    let mut current_gen = 0;

    while current_gen_growth > 0 {
        generations.push(current_gen_growth);
        current_gen += 1;
        current_gen_growth = generation_reproductivity(first_spawn_time, days, current_gen);
    }

    let mut seed = vec![1; generations[0] as usize];
    let mut total_growth: u64 = 1 + generations[0] as u64;

    for gen_growth in generations[1..].iter() {
        seed = generation_growth(&seed, *gen_growth as usize);

        total_growth += seed.iter().fold(0, |sum, n| sum + n) as u64;
    }
    
    total_growth
}

#[aoc(day6, part1)]
fn day6_1(input: &HashMap<i32, u64>) -> u64 {
    let days = 80;

    input
        .into_iter()
        .fold(0, |sum, (k, v)| sum + lanternfish_growth(*k, days) * v)
}

#[aoc(day6, part2)]
fn day6_2(input: &HashMap<i32, u64>) -> u64 {
    let days = 256;

    input
        .into_iter()
        .fold(0, |sum, (k, v)| sum + lanternfish_growth(*k, days) * v)
}

#[allow(unused_macros)]
macro_rules! generation_growth_tests {
    ($($name:ident: $value:expr,)*) => {
    $(
        #[test]
        fn $name() {
            let (seed, reproductions, expected) = $value;
            
            let result = generation_growth(&seed, reproductions);

            assert_eq!(expected, result);
        }
    )*
    }
}

#[allow(unused_macros)]
macro_rules! generation_reproductivity_tests {
    ($($name:ident: $value:expr,)*) => {
    $(
        #[test]
        fn $name() {
            let (first_spawn_time, days, gen, expected) = $value;
            
            let result = generation_reproductivity(first_spawn_time, days, gen);

            assert_eq!(expected, result);
        }
    )*
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    generation_growth_tests! {
        generation_growth_gen_2: (vec![1,1,1,1,1,1], 5, vec![1,2,3,4,5]),
        generation_growth_gen_3: (vec![1,2,3,4,5], 5, vec![1,3,6,10,15]),
    }

    generation_reproductivity_tests! {
        generation_reproductivity_days_18_gen_0: (1, 40, 0, 6),
        generation_reproductivity_days_18_gen_1: (1, 40, 1, 5),
    }

    #[test]
    fn test_day6_1() {
        let input = "3,4,3,1,2";
        
        let parsed_input = input_generator(input);
        assert_eq!(day6_1(&parsed_input), 5934);
    }

    #[test]
    fn test_day6_2() {
        let input = "3,4,3,1,2";

        let parsed_input = input_generator(input);
        assert_eq!(day6_2(&parsed_input), 26984457539);
    }
}
