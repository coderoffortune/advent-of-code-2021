#[aoc_generator(day3)]
fn input_generator(input: &str) -> Vec<Vec<u32>> {
    input
        .lines()
        .map(|l| l.trim().split_terminator("").skip(1).map(|bit| bit.parse::<u32>().unwrap_or(0)))
        .map(|l| l.collect())
        .collect()
}

fn traspose(input: &Vec<Vec<u32>>) -> Vec<Vec<u32>> {
    let mut trasposed: Vec<Vec<u32>> = Vec::new();

    let rows = input.len() as u32;
    let cols = input[0].len() as u32;

    for _ in 0..cols {
        trasposed.push(Vec::new());
    }

    for row in 0..rows {
        for col in 0..cols {
            trasposed[col as usize].push(input[row as usize][col as usize]);
        }
    }

    return trasposed;
}

fn filter_by_mask(data: &Vec<Vec<u32>>, index: i32, reverse: u32) -> Vec<u32> {
    if data.len() == 1 {
        return data.first().unwrap().to_vec();
    }

    let bitcount = data.iter().map(|row| row[index as usize]).fold(0_u32, |acc, bit| acc + bit);
    let bitmask = if bitcount as f32 >= (data.len() as f32 / 2.0) { 1 } else { 0 };
    
    let filtered_data = data.iter().filter(|row| row[index as usize] == (bitmask ^ reverse)).cloned().collect();

    return filter_by_mask(&filtered_data, index + 1, reverse)
}

fn binary_vec_to_u32(data: &Vec<u32>) -> u32 {
    data.iter().rev().enumerate().fold(0, |acc, (power, bit)| acc + bit * 2_u32.pow(power as u32))
}

#[aoc(day3, part1)]
fn day3_1(input: &Vec<Vec<u32>>) -> u32 {
    let trasposed_input: Vec<Vec<u32>> = traspose(input);

    let (gamma_rate, epsilon_rate) = trasposed_input.iter().rev().enumerate().fold(
        (0, 0),
        |(g, e), (power, row)| {
            if row.iter().sum::<u32>() >= ((row.len() as u32) / 2) {            
                return (g + 2_u32.pow(power as u32), e)
            } else {
                return (g, e + 2_u32.pow(power as u32))
            }
        }
    );

    return gamma_rate * epsilon_rate;
}

#[aoc(day3, part2)]
fn day3_2(input: &Vec<Vec<u32>>) -> u32 {
    let oxygen_generator = filter_by_mask(input, 0, 0);
    let c02_scrubber = filter_by_mask(input, 0, 1);
    
    let oxygen_generator_rating = binary_vec_to_u32(&oxygen_generator);
    let c02_scrubber_rating = binary_vec_to_u32(&c02_scrubber);

    return oxygen_generator_rating * c02_scrubber_rating;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_day3_1() {
        let input = "00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010";
                                       
        assert_eq!(day3_1(&input_generator(input)), 198);
    }

    #[test]
    fn test_day3_2() {
        let input = "00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010";
                                       
        assert_eq!(day3_2(&input_generator(input)), 230);
    }
}
