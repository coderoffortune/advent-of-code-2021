use std::collections::HashMap;
use either::*;

#[aoc_generator(day10)]
fn input_generator(input: &str) -> Vec<Vec<char>> {
    input
        .lines()
        .map(|l| l.trim().chars().map(|c| c).collect())
        .collect()
}

fn autocomplete(code: &Vec<char>) -> Either<Vec<char>, char> {
    let symbols: HashMap<char, char> = [('(', ')'), ('[', ']'), ('{', '}'), ('<', '>')].iter().cloned().collect();

    let mut open_chunks = Vec::new();

    for symbol in code {
        if symbols.contains_key(symbol) {
            open_chunks.push(symbol);

            continue;
        }

        if open_chunks.len() == 0 {
            return Right(*symbol)
        }
        let last_open_symbol = open_chunks.pop().unwrap();

        if symbols.get(last_open_symbol).unwrap() != symbol {
            return Right(*symbol)
        }
    }

    let autocomplete: Vec<char> = open_chunks
        .iter()
        .map(|symbol| symbols.get(symbol).unwrap())
        .map(|symbol| *symbol)
        .rev()
        .collect();
    
    return Left(autocomplete);
}

#[aoc(day10, part1)]
fn day10_1(input: &Vec<Vec<char>>) -> u64 {
    let values: HashMap<char, u64> = [(')', 3), (']', 57), ('}', 1197), ('>', 25137)].iter().cloned().collect();

    let score: u64 = input
        .iter()
        .flat_map(|code| autocomplete(code).right())
        .map(|symbol| values.get(&symbol).unwrap())
        .sum();

    score
}

#[aoc(day10, part2)]
fn day10_2(input: &Vec<Vec<char>>) -> u64 {
    let values: HashMap<char, u64> = [(')', 1), (']', 2), ('}', 3), ('>', 4)].iter().cloned().collect();

    let mut scores: Vec<u64> = input
        .iter()
        .flat_map(|code| autocomplete(code).left())
        .map(|closures| 
            closures
                .iter()
                .fold(0_u64, |acc, symbol| acc*5 + values.get(&symbol).unwrap())
        )
        .collect();

    scores.sort();

    scores[scores.len()/2]
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day10_1() {
        let input = "[({(<(())[]>[[{[]{<()<>>
            [(()[<>])]({[<{<<[]>>(
            {([(<{}[<>[]}>{[]{[(<()>
            (((({<>}<{<{<>}{[]{[]{}
            [[<[([]))<([[{}[[()]]]
            [{[{({}]{}}([{[{{{}}([]
            {<[[]]>}<{[{[{[]{()[[[]
            [<(<(<(<{}))><([]([]()
            <{([([[(<>()){}]>(<<{{
            <{([{{}}[<[[[<>{}]]]>[]]";
        
        let parsed_input = input_generator(input);
        assert_eq!(day10_1(&parsed_input), 26397);
    }

    #[test]
    fn test_day10_2() {
        let input = "[({(<(())[]>[[{[]{<()<>>
            [(()[<>])]({[<{<<[]>>(
            {([(<{}[<>[]}>{[]{[(<()>
            (((({<>}<{<{<>}{[]{[]{}
            [[<[([]))<([[{}[[()]]]
            [{[{({}]{}}([{[{{{}}([]
            {<[[]]>}<{[{[{[]{()[[[]
            [<(<(<(<{}))><([]([]()
            <{([([[(<>()){}]>(<<{{
            <{([{{}}[<[[[<>{}]]]>[]]";

        let parsed_input = input_generator(input);
        assert_eq!(day10_2(&parsed_input), 288957);
    }
}
