use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Line {
    a: Point,
    b: Point
}

#[aoc_generator(day5)]
fn input_generator(input: &str) -> Vec<Line> {
    let lines: Vec<Line> = input
        .lines()
        .map(|l| l.trim())
        .map(|l| {
            let points: Vec<Point> = l
                .split(" -> ")
                .map(|point| {
                    let coords: Vec<i32> = point.split(",").map(|coord| coord.parse::<i32>().unwrap()).collect();

                    Point {
                        x: coords[0],
                        y: coords[1]
                    }
                })
                .collect();

            Line {
                a: points[0],
                b: points[1]
            }
        })
        .collect();

    lines
}

fn line_points(line: &Line) -> Vec<Point> {
    let mut points = Vec::new();

    let start = line.a;
    let end = line.b;

    let y_direction = if start.y > end.y { -1 } else if start.y == end.y  { 0 } else { 1 };
    let x_direction = if start.x > end.x { -1 } else if start.x == end.x  { 0 } else { 1 };
    
    let x_distance = i32::abs(line.a.x - line.b.x) + 1;
    let y_distance = i32::abs(line.a.y - line.b.y) + 1;
    let distance = if x_distance >= y_distance { x_distance } else { y_distance };

    for i in 0..distance {
        let point = Point {
            x: start.x + i * x_direction,
            y: start.y + i * y_direction 
        };
        
        points.push(point);
    }

    points
}

#[aoc(day5, part1)]
fn day5_1(input: &Vec<Line>) -> usize {
    let mut points = HashMap::new();

    let lines_points: Vec<Point> = input
        .iter()
        .filter(|line| line.a.x == line.b.x || line.a.y == line.b.y)
        .flat_map(|line| line_points(line))
        .collect();

    for point in lines_points {
        *points.entry(point).or_insert(0) += 1;
    }

    let high_threat_coords = points.values().cloned().collect::<Vec<i32>>();

    high_threat_coords.iter().filter(|occurrences| **occurrences > 1).count()
}

#[aoc(day5, part2)]
fn day5_2(input: &Vec<Line>) -> usize {
    let mut points = HashMap::new();

    let lines_points: Vec<Point> = input
        .iter()
        .flat_map(|line| line_points(line))
        .collect();

    for point in lines_points {
        *points.entry(point).or_insert(0) += 1;
    }

    let high_threat_coords = points.values().cloned().collect::<Vec<i32>>();

    high_threat_coords.iter().filter(|occurrences| **occurrences > 1).count()
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day5_1() {
        let input = "0,9 -> 5,9
            8,0 -> 0,8
            9,4 -> 3,4
            2,2 -> 2,1
            7,0 -> 7,4
            6,4 -> 2,0
            0,9 -> 2,9
            3,4 -> 1,4
            0,0 -> 8,8
            5,5 -> 8,2";
        
        let parsed_input = input_generator(input);
        assert_eq!(day5_1(&parsed_input), 5);
    }

    #[test]
    fn test_day5_2() {
        let input = "0,9 -> 5,9
            8,0 -> 0,8
            9,4 -> 3,4
            2,2 -> 2,1
            7,0 -> 7,4
            6,4 -> 2,0
            0,9 -> 2,9
            3,4 -> 1,4
            0,0 -> 8,8
            5,5 -> 8,2";

        let parsed_input = input_generator(input);
        assert_eq!(day5_2(&parsed_input), 12);
    }
}
