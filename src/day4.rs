use regex::Regex;

struct Bingo {
    extractions: Vec<i32>,

    boards: Vec<Vec<Vec<i32>>>
}

fn extract_boards(source: &Vec<&str>) -> Vec<Vec<Vec<i32>>> {
    let mut boards: Vec<Vec<Vec<i32>>> = Vec::new();
    let mut board: Vec<Vec<i32>> = Vec::new();

    for n in 0..source.len() {
        if n % 5 == 0 {
            if board.len() != 0 {
                boards.push(board);
            }

            board = Vec::new();
        }

        let regex = Regex::new(r"\s+").unwrap();
        let source_row = regex.replace_all(source[n], " ");

        let row = source_row.split(" ").map(|num| num.parse::<i32>().unwrap()).collect();

        board.push(row);
    }

    boards.push(board);

    return boards
}

#[aoc_generator(day4)]
fn input_generator(input: &str) -> Bingo {
    let lines: Vec<&str> = input.lines().map(|l| l.trim()).filter(|l| l.len() > 0).collect();
    let mut boards = lines.to_vec();
    boards.drain(0..1);

    let bingo = Bingo {
        extractions: lines[0].split(",").map(|num| num.parse::<i32>().unwrap()).collect(),
        boards: extract_boards(&boards)
    };

    bingo
}

fn mark_board(board: &Vec<Vec<i32>>, extraction: i32) -> (bool, Vec<Vec<i32>>) {
    let new_board: Vec<Vec<i32>> = board.iter().map(|row| 
        row.iter().map(|col| if *col != extraction {*col} else {-1}).collect()
    ).collect();

    let winner_by_row: bool = new_board.iter().any(|row| row.iter().all(|num| *num == -1));

    let winner_by_col: bool = [0,1,2,3,4].iter().fold(false, |acc, index| 
        acc || (
            new_board[0][*index as usize] == -1 &&
            new_board[1][*index as usize] == -1 &&
            new_board[2][*index as usize] == -1 &&
            new_board[3][*index as usize] == -1 &&
            new_board[4][*index as usize] == -1
        )
    );

    return (winner_by_row || winner_by_col, new_board)
}

fn board_score(board: &Vec<Vec<i32>>) -> i32 {
    board.iter().fold(0, |score, row| {
        score + row.iter().fold(0, |row_score, col| if *col != -1 { row_score + *col } else { row_score })
    })    
}

#[aoc(day4, part1)]
fn day4_1(input: &Bingo) -> i32 {
    let boards: Vec<(i32, i32, i32)> = input.boards.iter().map(|board| {
        let mut current_board = board.to_vec();

        for (index, extraction) in input.extractions.iter().enumerate() {
            let (winner, new_board) = mark_board(&current_board, *extraction);
            current_board = new_board.to_vec();

            if winner {
                let score = board_score(&current_board);
                return (index as i32, *extraction, score);
            }
        };

        return (-1, -1, 0);
    }).collect();

    boards.iter().fold((9999, 0), |(index, score), board| if board.0 < index {
        (board.0, board.1 * board.2)
    } else {
        (index, score)
    }).1
}

#[aoc(day4, part2)]
fn day4_2(input: &Bingo) -> i32 {
    let boards: Vec<(i32, i32, i32)> = input.boards.iter().map(|board| {
        let mut current_board = board.to_vec();

        for (index, extraction) in input.extractions.iter().enumerate() {
            let (winner, new_board) = mark_board(&current_board, *extraction);
            current_board = new_board.to_vec();

            if winner {
                let score = board_score(&current_board);
                return (index as i32, *extraction, score);
            }
        };

        return (-1, -1, 0);
    }).collect();

    boards.iter().fold((0, 0), |(index, score), board| if board.0 > index {
        (board.0, board.1 * board.2)
    } else {
        (index, score)
    }).1
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day4_1() {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

        22 13 17 11  0
         8  2 23  4 24
        21  9 14 16  7
         6 10  3 18  5
         1 12 20 15 19
        
         3 15  0  2 22
         9 18 13 17  5
        19  8  7 25 23
        20 11 10 24  4
        14 21 16 12  6
        
        14 21 17 24  4
        10 16 15  9 19
        18  8 23 26 20
        22 11 13  6  5
         2  0 12  3  7";

        let parsed_input = input_generator(input);
        assert_eq!(day4_1(&parsed_input), 4512);
    }

    #[test]
    fn test_day4_2() {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

        22 13 17 11  0
         8  2 23  4 24
        21  9 14 16  7
         6 10  3 18  5
         1 12 20 15 19
        
         3 15  0  2 22
         9 18 13 17  5
        19  8  7 25 23
        20 11 10 24  4
        14 21 16 12  6
        
        14 21 17 24  4
        10 16 15  9 19
        18  8 23 26 20
        22 11 13  6  5
         2  0 12  3  7";

        let parsed_input = input_generator(input);
        assert_eq!(day4_2(&parsed_input), 1924);
    }
}
