#[aoc_generator(day7)]
fn input_generator(input: &str) -> Vec<i32> {
    let lifespans: Vec<i32> = input
        .trim()
        .split(",")
        .map(|lifespan| lifespan.trim().parse::<i32>().unwrap())
        .collect();

    lifespans
}

#[aoc(day7, part1)]
fn day7_1(input: &Vec<i32>) -> i32 {
    let max = input.iter().max().unwrap();
    
    let fuel_costs: Vec<i32> = (0..*max)
        .map(|value| input.iter().fold(0, |sum, x_pos| sum + (x_pos - value).abs()))
        .collect();

    *fuel_costs.iter().min().unwrap()
}

#[aoc(day7, part2)]
fn day7_2(input: &Vec<i32>) -> i32 {
    let max = input.iter().max().unwrap();

    let fuel_costs: Vec<i32> = (0..*max)
        .map(|value| 
            input.iter().fold(0, |sum, x_pos| {
                let distance = (x_pos - value).abs();

                sum + distance*(distance+1)/2
            })
        )
        .collect();

    *fuel_costs.iter().min().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_day7_1() {
        let input = "16,1,2,0,4,2,7,1,2,14";
        
        let parsed_input = input_generator(input);
        assert_eq!(day7_1(&parsed_input), 37);
    }

    #[test]
    fn test_day7_2() {
        let input = "16,1,2,0,4,2,7,1,2,14";

        let parsed_input = input_generator(input);
        assert_eq!(day7_2(&parsed_input), 168);
    }
}
